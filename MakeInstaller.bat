@echo off
setlocal

set projectDir=.
set WIX=C:\wixtoolset\

:: Check if a version argument is passed
if "%1"=="" (
    echo Error: No version argument passed && exit /b 1
) else (
    set version=%1
)

echo "Generate fragment file for application files"
"%WIX%heat.exe" dir "app" -cg INSTALLDIR_comp -gg -scom -sreg -sfrag -srd -dr INSTALLDIR -var var.ProjectDir -out "build\Product.Files.wxs"
"%WIX%candle.exe" "build\Product.Files.wxs" -out "build\_Product.Files.wixobj" -dProjectDir="%projectDir%\app"

echo "Create OCX"
"%WIX%heat.exe" dir "OCX" -cg OCX_comp -gg -var var.ProjectDir -out "build\OCX.wxs"
"%WIX%candle.exe" "build\OCX.wxs" -out "build\_OCX.wixobj" -dProjectDir="%projectDir%\OCX"

echo "Build BMW Advanced Tools Installer"
"%WIX%candle.exe" "Product.wxs" -out "build\_Product.wixobj" -dBUILD_GUID="36B1E5AB-048D-444A-950C-56B820F58712" -dBUILD_VERSION="%version%.0" -dBUILD_PROJECTDIR="%projectDir%\app" -ext WixUtilExtension -nologo
"%WIX%light.exe" "build\_OCX.wixobj" "build\_Product.Files.wixobj" "build\_Product.wixobj" -loc "Product.Loc-en.wxl" -cultures:en-US -ext WixUtilExtension -ext WixUIExtension -ext WixNetFxExtension -out "build\BMW_Advanced_Tools_%version%.msi" -nologo